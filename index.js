/*
	You can declare functions by using:
	1. function keyword
	2. function name
	3. open/close parenthesis
	4. open/close curly braces
*/
function sayHello(){
	console.log('Hello there!')
}
// You can invoke a function by calling its function name and including the parenthesis
sayHello()

// You can assign a function to a variable. The function would not be required
let sayGoodbye = function(){
	console.log('Goodbye')
}
//You can invoke a function inside a variable by using a variable name
sayGoodbye()

// You can also re-assign a function as a new value of a variable
sayGoodbye = function(){
	console.log('Au Revoir!')
}

sayGoodbye()


// Declaring a constant variable with a function as a value will not allow that function to be changes or re-assigned
const sayHelloInJapanese = function(){
	console.log('Ohayo!')
}

/*sayHelloInJapanese = function(){
	console.log('Kumusta!')
}*/

sayHelloInJapanese()


//Global Scope - You can use a variable inside a function if the  variable is declared outside of it
let action = 'Run'

function doSomethingRandom(){
	console.log(action)
}

doSomethingRandom()


/*
// Local/Function Scope - You cannot use a variable outside of a function if it is within the function scopy/curlybraces
function doSomethingRandom(){
	let action = 'Run'
	
}

console.log(action)
*/

// You can nest functions inside of a function as long as you invoke the child fuction within the scope of the parent function
function viewProduct(){

	console.log('Viewing a product')

	function addToCart(){
		console.log('Added product to cart')
	}
	addToCart()
}

viewProduct()

// Alert function is a built-in javascript function where we can show alerts to the user
function singASong(){
	alert('La la la')
}

singASong()

// Any statement like 'console.log' will run only after the alert has been closed if it is invoked below of the alert function
console.log('Clap clap clap')

// Prompt is a built-in javascript function that we can use to take input from the user
function enterUserName(){
	let UserName = prompt('Enter your username')
	console.log(UserName)
}

enterUserName()

// You can use a prompt outside of a function. Make sure to output the prompt value by assigning to a variable and using console.log
let userAge = prompt('Enter your age')
console.log(userAge)

//Alternative
//console.log(prompt('Enter your age'))
